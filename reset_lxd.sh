#!/bin/sh

apt remove -y --purge lxd lxd-client
zpool destroy default
rm -Rf /var/lib/lxd
ip link set lxdbr0 down
brctl delbr lxdbr0
apt-get install -y lxd
